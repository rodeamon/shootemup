﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour {

    private Transform move;
    public float turnSpeed;
    public float moveSpeed;

	// Use this for initialization
	void Start () {
        move = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKey(KeyCode.W)||Input.GetKey(KeyCode.UpArrow))
        {
            move.Translate(0,moveSpeed,0);
        };
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            move.Translate(0,-moveSpeed,0);
        };

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            move.Rotate (0, 0, turnSpeed);
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            move.Rotate(0, 0, - turnSpeed);
        }
    }
}
