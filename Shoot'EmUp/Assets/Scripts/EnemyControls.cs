﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControls : MonoBehaviour
{
    
    private Transform start;
    public float speed; 


    // Start is called before the first frame update
    void Start()
    {
        int objStartDir = Random.Range(0, 360);
        start.Rotate(0, 0, objStartDir);

    }


    // Update is called once per frame
    void Update()
    {
         start.Translate(0, speed, 0);
    }
}
